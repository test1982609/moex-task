FROM mcr.microsoft.com/dotnet/aspnet:6.0-alpine
ARG EXECUTABLE

WORKDIR /app
COPY /publish .

ENV ASPNETCORE_URLS "http://*:5000"
ENV ASPNETCORE_ENVIRONMENT "Staging"
CMD ["dotnet", "$EXECUTABLE"]